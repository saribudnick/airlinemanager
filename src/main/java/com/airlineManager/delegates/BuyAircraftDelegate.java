package com.airlineManager.delegates;

import com.airlineManager.models.Airline;
import com.airlineManager.repository.AircraftRepository;
import com.airlineManager.repository.AirlineRepository;
import com.airlineManager.models.Aircraft;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;

public class BuyAircraftDelegate {
	@Inject
	AircraftRepository aircraftRepository;
	@Inject
	private AirlineRepository airlineRepository;

	public synchronized ResponseEntity<String> execute(String serialNumber, String airlineName) {

		Aircraft aircraft = aircraftRepository.findBySerialNumber(serialNumber);
		Airline airline = airlineRepository.findByName(airlineName);
		Airline oldAirline = airlineRepository.findByName(aircraft.getAirline()
			.getName());
		if (aircraft.getPrice() > airline.getBudget()) {
			throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "aircraft price exceeds current balance", null);
		}
		if (!aircraft.getIsForSale()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format("aircraft %s is not for sale",
				serialNumber), null);
		}
		aircraft.setIsForSale(false);
		long price = aircraft.getPrice();
		oldAirline.setBudget(oldAirline.getBudget() + price);
		airline.setBudget(airline.getBudget() - price);
		aircraft.setAirline(airline);
		aircraftRepository.save(aircraft);
		return ResponseEntity.ok(null);
	}
}
