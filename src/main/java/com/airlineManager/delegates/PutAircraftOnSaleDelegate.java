package com.airlineManager.delegates;

import javax.inject.Inject;
import javax.persistence.EntityNotFoundException;

import com.airlineManager.models.Aircraft;
import com.airlineManager.repository.AircraftRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

public class PutAircraftOnSaleDelegate {
	@Inject
	private AircraftRepository aircraftRepository;

	public ResponseEntity<String> execute(String serialNumber) {

		try {
			Aircraft aircraft = aircraftRepository.getById(serialNumber);
			aircraft.setIsForSale(true);

			aircraftRepository.save(aircraft);
			return ResponseEntity.ok(String.format("Aircraft with serial Number %s is now for sale", serialNumber));
		}
		catch (EntityNotFoundException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Aircraft %s  Not Found",
				serialNumber), ex);
		}

	}
}
