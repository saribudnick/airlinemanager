package com.airlineManager.delegates;

import com.airlineManager.models.Airline;
import com.airlineManager.repository.AirlineRepository;
import org.springframework.http.ResponseEntity;

import javax.inject.Inject;
import java.util.List;

public class RetrieveAirlinesDelegate {
	@Inject
	private AirlineRepository airlineRepository;

	public ResponseEntity<List<Airline>> execute() {

		return ResponseEntity.ok(airlineRepository.findAll());
	}
}
