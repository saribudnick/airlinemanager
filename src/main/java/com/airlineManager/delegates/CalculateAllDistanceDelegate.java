package com.airlineManager.delegates;

import com.airlineManager.models.Airline;
import com.airlineManager.models.Location;
import com.airlineManager.repository.AirlineRepository;
import com.airlineManager.utility.Haversine;
import com.airlineManager.repository.LocationRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class CalculateAllDistanceDelegate {
	@Inject
	private AirlineRepository airlineRepository;
	@Inject
	LocationRepository locationRepository;

	public ResponseEntity<Map<String, Double>> execute(String name) {

		Airline airline = airlineRepository.findByName(name);
		if (airline == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("airlineName %s  Not Found",
				name), new ObjectNotFoundException("airline", name));
		}
		List<Location> locationList = locationRepository.findAll();
		Map<String, Double> calculatetedDistance = new HashMap<>();
		locationList.stream()
			.filter(Objects::nonNull)
			.forEach(location -> calculatetedDistance.put(location.getName(), Haversine.distance(airline.getHomeBaseLocation()
				.getAltitude(), airline.getHomeBaseLocation()
					.getLongitude(), location.getAltitude(), location.getLongitude())));
		return ResponseEntity.ok(calculatetedDistance);
	}
}
