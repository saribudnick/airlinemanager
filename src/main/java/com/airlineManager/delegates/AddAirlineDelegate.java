package com.airlineManager.delegates;

import javax.inject.Inject;

import com.airlineManager.models.Airline;
import com.airlineManager.models.Location;
import com.airlineManager.repository.AirlineRepository;
import com.airlineManager.repository.LocationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

public class AddAirlineDelegate {

	@Inject
	private AirlineRepository airlineRepository;
	@Inject
	private LocationRepository locationRepository;

	public ResponseEntity<String> execute(Airline airlineToBeAdded) {

		Airline airline = airlineRepository.findByName(airlineToBeAdded.getName());
		if (airline != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, String.format(
				"you are trying to add an already existing airline"), null);
		}
		Location location = locationRepository.findByName(airlineToBeAdded.getHomeBaseLocation()
			.getName());
		airline = airlineRepository.save(new Airline(airlineToBeAdded.getName(), airlineToBeAdded
			.getBudget(), location));
		return ResponseEntity.ok(String.format("new Airline %s was added", airline.getName()));
	}
}
