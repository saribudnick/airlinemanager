package com.airlineManager.delegates;

import com.airlineManager.models.Aircraft;
import com.airlineManager.repository.AircraftRepository;
import org.springframework.http.ResponseEntity;

import javax.inject.Inject;
import java.util.*;

public class RetrieveAllAvailableDestinationDelegate {
	@Inject
	private CalculateAllDistanceDelegate calculateAllDistanceDelegate;
	@Inject
	private AircraftRepository aircraftRepository;

	public ResponseEntity<Set<String>> execute(String airlineName) {

		Map<String, Double> calculatedDistances = calculateAllDistanceDelegate.execute(airlineName)
			.getBody();
		Aircraft aircraft = aircraftRepository.findFirstByAirline_NameOrderByMaxDistanceDesc(airlineName);
		Set<String> availableDestinationNames = new HashSet<>();
		calculatedDistances.forEach((name, distance) -> {
			if (distance <= aircraft.getMaxDistance()) {
				availableDestinationNames.add(name);
			}
		});
		return ResponseEntity.ok(availableDestinationNames);
	}
}
