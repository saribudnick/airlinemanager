package com.airlineManager.delegates;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.inject.Inject;

import com.airlineManager.models.Aircraft;
import com.airlineManager.repository.AircraftRepository;
import org.springframework.http.ResponseEntity;

public class GetAllAircraftForSaleDelegate {
	@Inject
	private AircraftRepository aircraftRepository;

	public ResponseEntity<Map<String, Long>> execute() {

		List<Aircraft> aircraftList = aircraftRepository.findByIsForSaleTrue();
		Map<String, Long> map = new HashMap<>();
		aircraftList.stream()
			.filter(Objects::nonNull)
			.forEach(aircraft -> map.put(aircraft.getSerialNumber(), (Long) aircraft.getPrice()));
		return ResponseEntity.ok(map);
	}
}
