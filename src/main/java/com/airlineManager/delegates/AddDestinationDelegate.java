package com.airlineManager.delegates;

import com.airlineManager.models.Location;
import com.airlineManager.repository.LocationRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;

public class AddDestinationDelegate {
	@Inject
	private LocationRepository locationRepository;

	public ResponseEntity<String> execute(String name, double altitude, double longitude) {

		Location location = locationRepository.findByName(name);
		if (location != null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "the location already exist", null);
		}
		location = new Location(name, longitude, altitude);
		locationRepository.save(location);
		return ResponseEntity.ok(location.toString());

	}
}
