package com.airlineManager.delegates;

import com.airlineManager.models.Aircraft;
import com.airlineManager.models.Airline;
import com.airlineManager.repository.AircraftRepository;
import com.airlineManager.repository.AirlineRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.server.ResponseStatusException;

import javax.inject.Inject;

public class AddAircraftDelegate {

	@Inject
	private AirlineRepository airlineRepository;

	@Inject
	private AircraftRepository aircraftRepository;

	public ResponseEntity<String> execute(String airlineName, int maxDistance, long price) {

		Airline airline = airlineRepository.findByName(airlineName);
		if (airline == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("airlineName %s  Not Found",
				airlineName), new ObjectNotFoundException("airline", airlineName));
		}
		Aircraft aircraft = new Aircraft(maxDistance, price, airline);
		aircraftRepository.save(aircraft);
		return ResponseEntity.ok(String.format("Aircraft %s was added.", aircraft.getSerialNumber()));

	}
}
