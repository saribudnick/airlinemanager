package com.airlineManager.repository;

import com.airlineManager.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, String> {
	Location findByName(String name);
}
