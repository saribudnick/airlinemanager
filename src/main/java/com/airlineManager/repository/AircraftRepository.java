package com.airlineManager.repository;

import com.airlineManager.models.Aircraft;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AircraftRepository extends JpaRepository<Aircraft, String> {

	List<Aircraft> findByIsForSaleTrue();

	Aircraft findBySerialNumber(String serialNumber);
	Aircraft findFirstByAirline_NameOrderByMaxDistanceDesc(String airline_name);
}
