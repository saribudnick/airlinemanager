package com.airlineManager.repository;

import com.airlineManager.models.Airline;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AirlineRepository extends JpaRepository<Airline, String> {

	Airline findByName(String name);


}
