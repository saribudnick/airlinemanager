package com.airlineManager.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "location")
public class Location {
	@Id
	private String name;
	@Column(name = "longitude")
	private double longitude;
	@Column(name = "altitude")
	private double altitude;

	public Location() {

	}

	@Override
	public String toString() {

		return "Location{" +
				"name='" + name + '\'' +
				", longitude=" + longitude +
				", altitude=" + altitude +
				'}';
	}

	public Location(String name, double longitude, double altitude) {

		this.name = name;
		this.longitude = longitude;
		this.altitude = altitude;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public double getAltitude() {

		return altitude;
	}

	public void setAltitude(double altitude) {

		this.altitude = altitude;
	}

	public double getLongitude() {

		return longitude;
	}

	public void setLongitude(double longitude) {

		this.longitude = longitude;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Location))
			return false;
		Location location = (Location) o;
		return Double.compare(location.getLongitude(), getLongitude()) == 0 &&
				Double.compare(location.getAltitude(), getAltitude()) == 0;
	}

	@Override
	public int hashCode() {

		return Objects.hash(getLongitude(), getAltitude());
	}
}
