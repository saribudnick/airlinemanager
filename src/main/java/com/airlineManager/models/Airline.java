package com.airlineManager.models;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "airline")
public class Airline {
	@Id
	private String name;
	@Column(name = "budget")
	private long budget;
	@OneToOne(optional = false)
	@JoinColumn(name = "location_name", referencedColumnName = "name")
	private Location homeBaseLocation;
	@OneToMany(mappedBy = "airline")
	@JsonManagedReference
	private List<Aircraft> aircraftList = new ArrayList<>();

	public Airline(String name, long budget, Location homeBaseLocation) {

		this.name = name;
		this.budget = budget;
		this.homeBaseLocation = homeBaseLocation;
	}

	public Airline() {

	}

	public String getName() {

		return this.name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public long getBudget() {

		return this.budget;
	}

	public void setBudget(long budget) {

		this.budget = budget;
	}

	public List<Aircraft> getAircraftList() {

		return this.aircraftList;
	}

	public void setAircraftList(List<Aircraft> aircraftList) {

		this.aircraftList = aircraftList;
	}

	public Location getHomeBaseLocation() {

		return this.homeBaseLocation;
	}

	public void setHomeBaseLocation(Location homeBaseLocation) {

		this.homeBaseLocation = homeBaseLocation;
	}

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (!(o instanceof Airline))
			return false;
		Airline airline = (Airline) o;
		return getBudget() == airline.getBudget() &&
				Objects.equals(getName(), airline.getName()) &&
				Objects.equals(getHomeBaseLocation(), airline.getHomeBaseLocation()) &&
				Objects.equals(getAircraftList(), airline.getAircraftList());
	}

	@Override
	public int hashCode() {

		return Objects.hash(getName(), getBudget(), getHomeBaseLocation(), getAircraftList());
	}

	public Airline removeAircraft(Aircraft aircraft) {

		aircraftList.remove(aircraft);
		aircraft.setAirline(null);
		return this;
	}
}
