package com.airlineManager.models;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonBackReference;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "aircraft")
public class Aircraft {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String serialNumber;
	@Column(name = "price")
	private long price;
	@Column(name = "maxDistance")
	private double maxDistance;
	@Column(name = "dateOfFirstUse")
	private Date dateOfFirstUse;
	@Column(name = "isForSale")
	private boolean isForSale;
	@ManyToOne()
	@JsonBackReference
	@JoinColumn(name = "airline_name", referencedColumnName = "name")
	private Airline airline;

	@Override
	public boolean equals(Object o) {

		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		Aircraft aircraft = (Aircraft) o;
		return price == aircraft.price &&
				maxDistance == aircraft.maxDistance &&
				isForSale == aircraft.isForSale &&
				serialNumber.equals(aircraft.serialNumber) &&
				dateOfFirstUse.equals(aircraft.dateOfFirstUse) &&
				airline.equals(aircraft.airline);
	}

	@Override
	public int hashCode() {

		return Objects.hash(serialNumber, price, maxDistance, dateOfFirstUse, isForSale, airline);
	}

	public Aircraft() {

	}

	public Aircraft(int maxDistance, long price, Airline airline) {

		this.serialNumber = UUID.randomUUID()
			.toString();
		this.price = price;
		this.maxDistance = maxDistance;
		this.dateOfFirstUse = new Date();
		this.isForSale = false;
		this.airline = airline;

	}

	public String getSerialNumber() {

		return this.serialNumber;
	}

	public void setSerialNumber(String serialNumber) {

		this.serialNumber = serialNumber;
	}

	public long getPrice() {

		return this.price * 1;
	}

	void setPrice(long price) {

		this.price = price;
	}

	public double getMaxDistance() {

		return this.maxDistance;
	}

	void setMaxDistance(double maxDistance) {

		this.maxDistance = maxDistance;
	}

	public Date getDateOfFirstUse() {

		return this.dateOfFirstUse;
	}

	void setDateOfFirstUse(Date dateOfFirstUse) {

		this.dateOfFirstUse = dateOfFirstUse;
	}

	public boolean getIsForSale() {

		return this.isForSale;
	}

	public void setIsForSale(boolean isForSale) {

		this.isForSale = isForSale;
	}

	public Airline getAirline() {

		return this.airline;
	}

	public void setAirline(Airline airline) {

		this.airline = airline;
	}

}
