package com.airlineManager.config;

import com.airlineManager.delegates.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DelegateConfig {

	@Bean
	public AddAirlineDelegate addAirlineDelegate() {

		return new AddAirlineDelegate();
	}

	@Bean
	public AddAircraftDelegate addAircraftDelegate() {

		return new AddAircraftDelegate();
	}

	@Bean
	public RetrieveAirlinesDelegate retrieveAirlinesDelegate() {

		return new RetrieveAirlinesDelegate();
	}

	@Bean
	public PutAircraftOnSaleDelegate putAircraftOnSaleDelegate() {

		return new PutAircraftOnSaleDelegate();
	}

	@Bean
	public GetAllAircraftForSaleDelegate getAllAircraftForSaleDelegate() {

		return new GetAllAircraftForSaleDelegate();
	}

	@Bean
	public BuyAircraftDelegate buyAircraftDelegate() {

		return new BuyAircraftDelegate();
	}
	@Bean
	public CalculateAllDistanceDelegate calculateAllDistanceDelegate() {

		return new CalculateAllDistanceDelegate();
	}
	@Bean
	public AddDestinationDelegate addDestinationDelegate() {

		return new AddDestinationDelegate();
	}

	@Bean
	public RetrieveAllAvailableDestinationDelegate retrieveAllAvailableDestinationDelegate() {

		return new RetrieveAllAvailableDestinationDelegate();
	}
}
