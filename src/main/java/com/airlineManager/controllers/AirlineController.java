package com.airlineManager.controllers;

import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.inject.Inject;

import com.airlineManager.delegates.*;
import com.airlineManager.models.Airline;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class AirlineController {
	@Inject
	private AddAirlineDelegate addAirlineDelegate;
	@Inject
	private RetrieveAirlinesDelegate retrieveAirlinesDelegate;
	@Inject
	private AddAircraftDelegate addAircraftDelegate;
	@Inject
	private PutAircraftOnSaleDelegate putAircraftOnSaleDelegate;
	@Inject
	private GetAllAircraftForSaleDelegate getAllAircraftForSaleDelegate;
	@Inject
	private BuyAircraftDelegate buyAircraftDelegate;
	@Inject
	private CalculateAllDistanceDelegate calculateAllDistanceDelegate;
	@Inject
	private AddDestinationDelegate addDestinationDelegate;
	@Inject
	private RetrieveAllAvailableDestinationDelegate retrieveAllAvailableDestinationDelegate;

	@RequestMapping(value = "/addAirline", method = RequestMethod.POST)
	public ResponseEntity<String> addAirline(@RequestBody(required = false) Airline airline) {

		return addAirlineDelegate.execute(airline);

	}

	@RequestMapping(value = "/retrieveAllAirlines", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<Airline>> retrieveAllAirlines() {

		return retrieveAirlinesDelegate.execute();

	}

	@RequestMapping(value = "/addDestination", params = { "locationName", "longitude", "altitude" }, method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> addDestination(@RequestParam String locationName, @RequestParam double longitude,
			@RequestParam double altitude) {

		return addDestinationDelegate.execute(locationName, altitude, longitude);

	}

	@RequestMapping(value = "/addAircraft", params = { "airlineName", "maxDistance", "price" }, method = RequestMethod.POST)
	public ResponseEntity<String> addAircraft(@RequestParam String airlineName,

			@RequestParam int maxDistance, @RequestParam long price) {

		return addAircraftDelegate.execute(airlineName, maxDistance, price);
	}

	@RequestMapping(value = "/putAircraftOnSale/serialNumber/{serialNumber}", method = RequestMethod.PUT)
	public ResponseEntity<String> putAircraftOnSale(@PathVariable String serialNumber) {

		return putAircraftOnSaleDelegate.execute(serialNumber);
	}

	@RequestMapping(value = "/getAllAircraftForSale", method = RequestMethod.GET)
	public ResponseEntity<Map<String, Long>> getAllAircraftForSale() {

		return getAllAircraftForSaleDelegate.execute();
	}

	@RequestMapping(value = "/buyAircraft", params = { "serialNumber", "airlineName" }, method = RequestMethod.PUT)
	public ResponseEntity<String> buyAircraft(@RequestParam String serialNumber, @RequestParam String airlineName) {

		return buyAircraftDelegate.execute(serialNumber, airlineName);
	}

	@RequestMapping(value = "/calculateAllDistance", params = { "airlineName" }, method = RequestMethod.GET)
	public ResponseEntity<Map<String, Double>> calculateAllDistance(@RequestParam String airlineName) {

		return calculateAllDistanceDelegate.execute(airlineName);
	}

	@RequestMapping(value = "/retrieveAllAvailableDestinations", params = { "airlineName" }, method = RequestMethod.GET)
	public ResponseEntity<Set<String>> retrieveAllAvailableDestinations(@RequestParam String airlineName) {

		return retrieveAllAvailableDestinationDelegate.execute(airlineName);
	}
}
